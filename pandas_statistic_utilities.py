def get_general_statistics(pandas_data_frame):
    pandas_data_frame = pd.to_numeric(pandas_data_frame)
    return pandas_data_frame.describe()

def get_standard_deviation(pandas_data_frame):
    return pandas_data_frame.std()

def get_average(pandas_data_frame):
    return pandas_data_frame.mean()

def get_mode(pandas_data_frame):
    return pandas_data_frame.mode()

def get_median(pandas_data_frame):
    return pandas_data_frame.median()

def get_kurtosis(pandas_data_frame):
    return pandas_data_frame.kurtosis()

def get_skew(pandas_data_frame):
    return pandas_data_frame.skew()

def get_iqr(quant25, quant75):
    return quant75 - quant25

def get_lower_boudary(quant25, iqr):
    return quant25 - 1.5 * iqr

def get_higher_boudary(quant75, iqr):
    return quant75 + 1.5 * iqr

def count_lower_boudary_outliers(pandas_data_frame, lower_boundary):
    return len(pandas_data_frame[(pandas_data_frame < lower_boundary)])

def count_higher_boudary_outliers(pandas_data_frame, higher_boundary):
    return len(pandas_data_frame[(pandas_data_frame > higher_boundary)])
